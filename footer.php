

<footer class="footer">

	<div class="container container-col-footer">
		<div class="col-footer">
			<?php dynamic_sidebar('sidebar_reseaux'); ?>
		</div>
		<div class="col-footer">
			<?php dynamic_sidebar('sidebar_solutions'); ?>
		</div>
		<div class="col-footer">
			<?php dynamic_sidebar('sidebar_agences'); ?>
		</div>
		<div class="col-footer">
			<?php dynamic_sidebar('sidebar_quisommesnous'); ?>
		</div>
		<div class="col-footer">
			<?php dynamic_sidebar('sidebar_travailleravecnous'); ?>
		</div>
	</div>

	<div class="container">
		<div class="col-2-2">
			<?php dynamic_sidebar('sidebar_credit'); ?>
		</div>
	</div>

	<div class="bg-dark-blue mt-50">
		<div class="container">
			<div class="col-2-2">
				<?php dynamic_sidebar('sidebar_mentions'); ?>
			</div>
		</div>
	</div>

		
</footer>



<script src="<?php bloginfo( 'template_url' ); ?>/js/init.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
