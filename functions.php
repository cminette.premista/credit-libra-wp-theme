<?php
// -----------------------------------------------
// Débugger symfony
// -----------------------------------------------
$composer_autoload = ABSPATH . "/vendor/autoload.php";
if (file_exists($composer_autoload)) {
    require_once $composer_autoload;
}


// -----------------------------------------------
// Custom backoffice css
// -----------------------------------------------
function customAdmin() 
{
    $url = site_url();
    $url = $url . '/wp-content/themes/canellecrea/css/styles-admin.min.css';
    echo '<!-- custom admin css -->
          <link rel="stylesheet" type="text/css" href="' . $url . '" />
          <!-- /end custom adming css -->';
}
add_action('admin_head', 'customAdmin');



// -----------------------------------------------
// Auteur
// -----------------------------------------------
function admin_footer()
{
	echo "Site réalisé par&nbsp;<a href='https://www.canellecrea.com' target='_blank'>Canellecrea</a>";
}
add_filter('admin_footer_text', 'admin_footer');


// -----------------------------------------------
// Widgets
// -----------------------------------------------
if ( function_exists('register_sidebar') ) {
$sidebar_menu = array(
'name'		=>'sidebar_menu',
'id'        => 'sidebar_menu',
'before_widget' => '<nav role="navigation" class="nav">',
'after_widget' 	=> '</nav>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_mentions = array(
'name'		=> 'sidebar_mentions',
'id'        => 'sidebar_mentions',
'before_widget' => '<div class="widget sidebar_mentions">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_reseaux = array(
'name'		=>'sidebar_reseaux',
'id'        => 'sidebar_reseaux',
'before_widget' => '<div class="widget sidebar_reseaux">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_solutions = array(
'name'		=>'sidebar_solutions',
'id'        => 'sidebar_solutions',
'before_widget' => '<div class="widget sidebar_solutions">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_agences = array(
'name'		=>'sidebar_agences',
'id'        => 'sidebar_agences',
'before_widget' => '<div class="widget sidebar_agences">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_quisommesnous = array(
'name'		=>'sidebar_quisommesnous',
'id'        => 'sidebar_quisommesnous',
'before_widget' => '<div class="widget sidebar_quisommesnous">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_travailleravecnous = array(
'name'		=>'sidebar_travailleravecnous',
'id'        => 'sidebar_travailleravecnous',
'before_widget' => '<div class="widget sidebar_travailleravecnous">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);
$sidebar_credit = array(
'name'		=>'sidebar_credit',
'id'        => 'sidebar_credit',
'before_widget' => '<div class="widget sidebar_credit">',
'after_widget' 	=> '</div>',
'before_title' 	=> '<h3>',
'after_title' 	=> '</h3>',
);


register_sidebar($sidebar_menu);
register_sidebar($sidebar_mentions);
register_sidebar($sidebar_reseaux);
register_sidebar($sidebar_solutions);
register_sidebar($sidebar_agences);
register_sidebar($sidebar_quisommesnous);
register_sidebar($sidebar_travailleravecnous);
register_sidebar($sidebar_credit);
}



// -----------------------------------------------
// Ajouter favicon BO WP
// -----------------------------------------------
function add_favicon() {
	$favicon_url = get_stylesheet_directory_uri() . '/images/favicon.ico';
  echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

// Now, just make sure that function runs when you're on the login page and admin pages  
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');


// -----------------------------------------------
// Full width with Gutenberg
// -----------------------------------------------
function mytheme_setup_theme_supported_features() {

	// Format large
	add_theme_support( 'align-wide' );

}
add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );

// -----------------------------------------------
// Augmenter taille upload
// -----------------------------------------------
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


//Enlever les accents sur les images
add_filter('sanitize_file_name', 'remove_accents' );


//Resize les categories dans l'admin wordpress
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
	#edittag{
		max-width:100%;
	}
  </style>';
}


// -----------------------------------------------
// Colors Gutenberg
// -----------------------------------------------
add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'green', 'canellecrea_child' ),
		'slug'  => 'green',
		'color'	=> '#8EBF21',
	),
	array(
		'name'  => __( 'blue-dark', 'canellecrea_child' ),
		'slug'  => 'blue-dark',
		'color' => '#00384F',
	),
	array(
		'name'  => __( 'blue', 'canellecrea_child' ),
		'slug'  => 'blue',
		'color' => '#054C6A',
	),
	array(
		'name'  => __( 'blue-demi-light', 'canellecrea_child' ),
		'slug'  => 'blue-demi-light',
		'color' => '#066289',
	),
	array(
		'name'  => __( 'yellow', 'canellecrea_child' ),
		'slug'  => 'yellow',
		'color' => '#FFE93C',
	),
	array(
		'name'  => __( 'grey', 'canellecrea_child' ),
		'slug'  => 'grey',
		'color' => '#707070',
	),
	array(
		'name'  => __( 'dark', 'canellecrea_child' ),
		'slug'  => 'dark',
		'color' => '#1A2022',
	),
));




// -----------------------------------------------
// Function creates post duplicate as a draft and redirects then to the edit post screen
// -----------------------------------------------
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * Nonce verification
	 */
	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Dupliquer</a>';
	}
	return $actions;
}
 
add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );


// Supprimer les flux rss des commentaires
function remove_comments_rss( $for_comments ) {
    return;
}
add_filter('post_comments_feed_link','remove_comments_rss');

remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Supprimer api json
add_filter('rest_jsonp_enabled', '_return_false');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

// Ajouter Featured image aux posts
add_theme_support( 'post-thumbnails' );

// Ne pas charger le css par défaut des gallery wordpress 3.5
add_filter( 'use_default_gallery_style', '__return_false' );


// Supprime les classes inutiles
// add_filter('nav_menu_css_class', 'cssattr_filter', 100, 1);
// function cssattr_filter($var) {
//     if(!is_array($var)) return;
//     $current_indicators = array('current', 'current-parent', 'current-menu-item', 'current-menu-parent', 'current_page_item', 'current_page_parent', 'facebook', 'twitter', 'panier-item', 'recherche-woo');
//     $newArr = array();
//     foreach($var as $el){
//         //check if it contains an ID or it's indicating the current page
//         if ((preg_match('#[0-9]#',$el))||in_array($el, $current_indicators)){
//             array_push($newArr, $el);
//         }
//     }
//     return $newArr;
// }








// CSS pour l'éditeur
add_editor_style();

// Shortcode dans les widgets
add_filter('widget_text', 'do_shortcode');

// Toujours masquer la barre d'outil du Front
function my_function_admin_bar(){
    return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

// Ajout de styles dans l'éditeur
function my_mce_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter('mce_buttons', 'my_mce_buttons');
function mce_mod( $init ) {
    $style_formats = array(
        array(
            'title' => 'Bouton',
            'selector' => 'a',
            'classes' => 'btn'
        ),
        array(
            'title' => 'Tableau',
            'selector' => 'table',
            'classes' => 'table'
        )
    );
    $init['style_formats'] = json_encode( $style_formats );
    return $init;
}
add_filter('tiny_mce_before_init', 'mce_mod');

// ----------------------------
// Date au format Français
// ----------------------------
function dateFR ($get_date){

    $get_date = DateTime::createFromFormat('Ymd', $get_date);

    // $jour_semaine = $get_date->format('D');
    $jour = $get_date->format('d');
    $mois = $get_date->format('m');
    $annee = $get_date->format('Y');

    $date = $jour.'/'.$mois.'/'.$annee;
    return $date;

}