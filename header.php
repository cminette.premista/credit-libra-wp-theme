<!DOCTYPE HTML>
<html lang="FR-fr">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<title><?php wp_title(''); ?></title>
	<?php if (is_404()) { echo '<meta name="robots" content="noindex,noarchive">'; } ?>

	<link rel="canonical" href="<?php bloginfo( 'url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/images/favicon.png">
    <link rel="stylesheet" media="all" href="<?php bloginfo( 'template_url' ); ?>/js/fancybox-master/dist/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.css" />
	<link rel="stylesheet" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/styles.min.css" />

	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/fancybox-master/dist/jquery.fancybox.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/tarteaucitron/tarteaucitron.js"></script>
	<script src="https://unpkg.com/scrollreveal"></script>
	<script src="https://kit.fontawesome.com/caecf0fdf3.js" crossorigin="anonymous"></script>
	
	<script type="text/javascript">
	tarteaucitron.init({
			"hashtag": "#tarteaucitron", /* Ouverture automatique du panel avec le hashtag */
			"highPrivacy": false, /* désactiver le consentement implicite (en naviguant) ? */
			"orientation": "bottom", /* le bandeau doit être en haut (top) ou en bas (bottom) ? */
			"adblocker": false, /* Afficher un message si un adblocker est détecté */
			"showAlertSmall": true, /* afficher le petit bandeau en bas à droite ? */
			"cookieslist": true, /* Afficher la liste des cookies installés ? */
			"removeCredit": false, /* supprimer le lien vers la source ? */
			"handleBrowserDNTRequest": false, /* Deny everything if DNT is on */
			//"cookieDomain": ".example.com" /* Nom de domaine sur lequel sera posé le cookie pour les sous-domaines */
	});
	</script>
	
	<?php wp_head(); ?>

</head>

<body <?php body_class('class-name'); ?>>

<header class="header">

	<div class="container">
		<div class="col-2-2">

			<a href="<?php bloginfo( 'url' ); ?>" class="logo">
				<img src="<?php bloginfo( 'template_url' ); ?>/images/logo-credit-libra.png" alt="<?php bloginfo( 'name' ); ?>" />
			</a>
			
			<?php dynamic_sidebar('sidebar_menu'); ?>
			
			<div class="mobile-nav-contener">
				<span id="mobile-nav">
					<span class="content"></span>
				</span>
			</div>
		
		</div>
	</div>

</header>

<div id="mobile-nav-container">
	<span class="close"><i class="far fa-times-circle"></i></span>
</div>

<div class="box-search">
		
	<div class="row">
		<form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo( 'url' ); ?>">
			<div>
				<label class="screen-reader-text" for="s">Rechercher</label>
			</div>
			<div>
				<input type="text" value="" name="s" id="s" placeholder="Ex. Simuler un crédit">
			</div>
			<div>
				<input type="submit" id="searchsubmit" value="Rechercher">
			</div>
		</form>
	</div>

	<div class="row">
		<span class="search-close"><i class="far fa-times-circle"></i></span>
	</div>

</div>


<section class="content-page" role="main">
