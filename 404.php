<?php get_header(); ?>
    <article>

		<div class="container">
            <div class="col-2-2">
                <h1 class="align-center">Page introuvable</h1>
            </div>
        </div>

        <div class="container">
            <div class="col-2-2 align-center">
                <h2>Ooups, veuillez nous excuser...</h2>
                <p>La page que vous avez demandée n'existe pas, ou a été déplacée.</p>
                <div class="btn">
					<a href="<?php echo site_url();?>" title="Retour à la page d'accueil">Retour à la page d'accueil</a>
				</div>
            </div>
        </div>
    </article>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
