<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article>

		<div class="container">
			<div class="col-2-2 style-h1">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>

		

        <div class="container-pt">
            <div class="col-2-2 first-single-img">
                <?php if (has_post_thumbnail()) :
				$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
				$post_image = $post_image[0];
				$post_image_full = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				$post_image_full = $post_image_full[0];
               	?>
					<a href="<?php echo $post_image; ?>" class="fancybox">
						<img src="<?php echo $post_image_full; ?>" alt="<?php the_title(); ?>" />
					</a>
				<?php else: ?>
					<a href="<?php bloginfo( 'template_url' ); ?>/images/placeholder.jpg" class="fancybox">
						<img src="<?php bloginfo( 'template_url' ); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />
					</a>
                <?php endif; ?>
            </div>
		</div>

		<div class="container-pt">
			<div class="col-2-2">
				<?php the_content(); ?>
			</div>
		</div>

		<div class="container-pb">
			<div class="col-2-2 link-cat-tag">

				<p>
					<strong>Categorie : </strong>
					<?php 
					$category = get_the_category();
					$allcategory = get_the_category(); 
					foreach ($allcategory as $category):
					?>
						<a href="<?php bloginfo( 'url' ); ?>/<?php echo $category->slug; ?>"><?php echo $category->cat_name; ?></a>
					<?php endforeach; ?>
				</p>

				<p>
					<strong>Tags : </strong>
					<?php echo show_tags(); ?>
				</p>

			</div>
		</div>
        
        <div class="container">
            <div class="col-2-2">
                <?php
                    // Current category
                    $category = get_the_category();
                    $current_cat_name = $category[0]->name;
                    $current_cat_slug = $category[0]->slug;
                    $current_cat_slug = site_url().'/'.$current_cat_slug;
				?>
				<div class="btn">
					<a href="<?php echo $current_cat_slug; ?>" class="btn">Voir toutes les réalisations</a>
				</div>
            </div>
		</div>
		
		<?php 
		$next_post_link_url = get_permalink( get_adjacent_post(false,'',false) ); 
		$prev_post_link_url = get_permalink( get_adjacent_post(false,'',true) );
		?>

		<nav class="container nav-post">
			<div class="col-1-2 prec-nav-post">
				<a href="<?php echo $next_post_link_url; ?>" title="Voir la précédente réalisation">Préc.</a>
			</div>
			<div class="col-1-2 next-nav-post">
				<a href="<?php echo $prev_post_link_url; ?>" title="Voir la prochaine réalisation">Suiv.</a>
			</div>
		</nav>

	



    </article>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
