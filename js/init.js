// --------------------------------------
// Scroll reveal
// --------------------------------------
ScrollReveal().reveal('.wp-block-column', { 
		scale: 0.85,
		duration: 1000, 
	}
);

// ScrollReveal().reveal('.xxx', { 
// 		scale: 2,
// 		duration: 4000, 
// 	}
// );


// --------------------------------------
// Anti clic droit
// --------------------------------------
// document.addEventListener("contextmenu", function(e){
// e.preventDefault();
// //alert('Page protégée !'); 
// }, false);



// --------------------------------------
// Autoriser uniquement numéro input phone wpforms
// --------------------------------------
$(".allownumericwithoutdecimal input").on("keypress keyup blur",function (event) {    
	$(this).val($(this).val().replace(/[^\d].+/, ""));
	 if ((event.which < 48 || event.which > 57)) {
		 event.preventDefault();
	 }
});




// --------------------------------------
// JS tarte au citron RGPD
// --------------------------------------

(tarteaucitron.job = tarteaucitron.job || []).push('jsapi');

// --------------------------------------
// SEARCH
// --------------------------------------
$('.item-search').on( "click", function() {
    $('.box-search').toggleClass('open-search');

});
$('.search-close').on( "click", function() {
    $('.box-search').removeClass('open-search');
});

// --------------------------------------
// MOBILE NAV
// --------------------------------------

// Close mobile nav
function toggle_close_menu(){
    $(".header").toggleClass("active");
  $('#mobile-nav-container').toggleClass('mobile-nav-open');
  $('.mobile-nav-contener-open').toggleClass('active');
}

// Clone mobile nav
$("#mobile-nav-container").append($(".nav .menu").clone());

// Toggle menu open / close
$('#mobile-nav, #mobile-nav-open, .close').on( "click", function() {
    $('.header').toggleClass('active');
    $('#mobile-nav-container').toggleClass('mobile-nav-open');
    $('.mobile-nav-contener-open').toggleClass('active');
});

// Accordeon Mobile
jQuery('body').on('click', '#mobile-nav-container ul li a', function(e) {
    if (/#/.test(this.href)) {
        e.preventDefault();
        $(this).next(".sub-menu").slideToggle('slow');
    }
});
$('#mobile-nav-container ul li a').each(function(){
	if (/#/.test(this.href)) {
        $(this).addClass('open-sub');
    }
});



// --------------------------------------
// Fancybox
// --------------------------------------

// Ajouter une classe aux liens des images
$('a[href*=".png"]').addClass("fancybox");
$('a[href*=".jpg"]').addClass("fancybox");
$('a[href*=".gif"]').addClass("fancybox");

$(document).ready(function () {
  $(".gallery a").fancybox({
    openEffect: "fade",
    closeEffect: "fade",
    padding: 0,
  });
  $(".fancybox").fancybox({
    openEffect: "fade",
    closeEffect: "fade",
    padding: 0,
  });

  // Ajouter l'attribut rel aux liens des galeries
  $(".blocks-gallery-grid, .woocommerce-product-gallery__wrapper").each(function (g) {
    $("a", this).attr("data-fancybox", function (i, attr) {
      return attr + " gallery-" + g;
    });
  });



  // --------------------------------------
  // AUTRES
  // --------------------------------------
  $("p:empty").remove();
});




//Header et header fixe
$(window).scroll(function(){
	var ScrollTop = parseInt($(window).scrollTop());
	if (ScrollTop > 100) {
		$(".header").css("top", '-100px');
	}else{
		$(".header").css("top", 0);
	}

    if (ScrollTop > 500) {
		$(".header").addClass("header-fixe");
		$(".header-fixe").css("top", 0);

    } else {
		$(".header-fixe").css("top", '-100px');
	}
	
	if(ScrollTop < 500) {
		$(".header-fixe").css("top", '-100px');
	}

	if(ScrollTop < 200) {
		$(".header").removeClass("header-fixe");
	}
});


//Ouvrir les pdfs dans nouvelle fenetre
$("a[target!='_system'][href$='.pdf']").attr("target", "_system");