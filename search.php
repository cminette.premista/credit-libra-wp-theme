<?php 
get_header(); 
?>

		<div class="bg-img-header">
			<div class="container">
				<div class="col-2-2">
					<h1>Recherche</h1>
				</div>
			</div>
		</div>

	
		<div class="container">
			<div class="col-2-2">

				<?php
				$s = get_search_query();
				$args = array(
					's' =>$s
				);
				// The Query
				$the_query = new WP_Query( $args ); ?>

				<?php if ( $the_query->have_posts() ): ?>

					<h2>Résultat de recherche pour : <?php echo  get_query_var('s'); ?></h2>

					<ul>
					<?php while ( $the_query->have_posts() ) {
						$the_query->the_post();
								?>
									<li>
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</li>
								<?php
					} ?>
					</ul>

				<?php else: ?>

					<h2>Aucun résultat</h2>
					<div class="alert alert-info">
						<p>Nous sommes désolé, nous n'avons rien trouvé pour cette recherche. Essayez un terme différent.</p>
					</div>

				<?php endif; ?>

			</div>
		</div>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
