<?php 
get_header(); 

$term = get_queried_object();
?>

    	<div class="container">
			<div class="col-2-2">
				<h1><?php single_cat_title(); ?></h1>
			</div>
		</div>

		<?php /* Catégorie description ----------------------- */ ?>
		<?php if(!empty(category_description())): ?>
			<div class="container">
				<div class="col-2-2">
					<?php echo category_description(); ?> 
				</div>
			</div>
		<?php endif; ?>




		<div class="container">
			<div class="col-2-2 wrap-item">

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article class="blog-item">

				<a href="<?php echo get_permalink(); ?>" title="Lire la suite">
					<div class="img-item">
						<?php 
						if (has_post_thumbnail()) : 
						$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
						$post_image = $post_image[0];
						?>
							<img src="<?php echo $post_image; ?>" alt="<?php the_title(); ?>" />
						<?php else: ?>
							<img src="<?php bloginfo( 'template_url' ); ?>/images/placeholder.jpg" alt="<?php the_title(); ?>" />
						<?php endif; ?>
					</div>
				</a>

				<div class="text-item">
					<h2><?php the_title(); ?></h2>
					<!-- <p><?php echo wp_trim_words( get_the_content(), 30, '...' ); ?></p> -->
					<!-- <a href="<?php echo get_permalink(); ?>" title="Lire l'article" class="btn">Lire la suite</a> -->
				</div>
				
			</article>
			<?php endwhile; ?>
			<?php endif; ?>


			</div>
		</div>

		<!-- <?php if ($wp_query->max_num_pages > 1): // check if the max number of pages is greater than 1  ?>
		<nav class="prev-next-posts">
			<div class="prev-posts-link">
				<?php echo get_next_posts_link( 'Older Articles', $wp_query->max_num_pages ); // display older posts link ?>
			</div>
			<div class="next-posts-link">
				<?php echo get_previous_posts_link( 'Newer Articles' ); // display newer posts link ?>
			</div>
		</nav>
		<?php endif; ?> -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
