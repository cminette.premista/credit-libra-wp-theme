<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article>


        <?php if($post->post_content!=="") : ?>
            <div class="container-nopad">
                <div class="col-2-2">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endif; ?>

        
    </article>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
